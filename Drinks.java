import java.util.ArrayList;
import java.util.List;

public class Drinks /* TODO: implements ...complétez... */ {

    List<MenuItem> items;

    /*
     * TODO: Complètez la classe DrinksIterator. Si vous estimez qu'il n'y a pas
     * besoins de cette classe, vous êtes libres de faire l'implémentation d'une
     * autre manière.
     */
    private static class DrinksIterator /* implements ...complétez... */ {

    }

    public Drinks() {
        this.items = new ArrayList<>();
        this.items.add(new MenuItem("Bière blonde", 5.0f));
        this.items.add(new MenuItem("Bière blanche", 6.0f));
        this.items.add(new MenuItem("Bière IPA", 6.5f));
        this.items.add(new MenuItem("Pastis", 5.0f));
    }

    /* TODO: ...complétez... */

}